# Integration testing

Integration tests play an important role in your test suite. Regardless of how simple your code is, it’s still important to verify how it works in integration
with other subsystems. It’s also crucial to balance the number of unit and integration tests.

## Main concept

In general, integration tests verify how your system works in integration with its dependencies – normally *out-of-process*. Integration testing takes as its input modules that have been unit tested, groups them in larger aggregates, applies tests defined in an integration test plan to those aggregates, and delivers as its output the integrated system ready for system testing.

Integration tests go through a larger amount of code (both your code and the code of the libraries used by the application), which makes them better than unit tests at protecting against regressions. They are also more detached from the production code and therefore have better resistance to refactoring.

## Integration vs Unit testing

The ratio between unit and integration tests can differ depending on the project's specifics, but the general rule of thumb is the following: check as many of the business scenario's edge cases as possible with unit tests; use integration tests to cover one happy path, as well as any edge cases that can't be covered by unit tests.

>A **happy path** is a successful execution of a business scenario. An **edge case** is when the business scenario execution results in an error.

>See more about Integration vs Unit testing [here](./testing-patterns.md#unit-vs-integration-testing).

## Testing out-of-process dependencies

There are two ways to implement such verification: use the real out-of-process dependency, or replace that dependency with a mock. All out-of-process dependencies fall into two categories:

- Managed dependencies (*out-of-process dependencies you have full control over*): These dependencies are only accessible through your application; interactions with them aren't visible to the external world. External systems normally don't access your database directly; they do that through the API your application provides, e.g. database. 
- Unmanaged dependencies (*out-of-process dependencies you don't have full control over*): Interactions with such dependencies are observable externally, e.g. an SMTP server and a message bus: both produce side effects visible to other applications.

Note that communications with managed dependencies are implementation details. Conversely, communications with unmanaged dependencies are part of your system's observable behavior. Therefore, use **real instances** of managed dependencies; replace unmanaged dependencies with **mocks**.

## Integration testing and UnityEngine

Integration testing is a great process for testing UnityEngine API usage, interaction, and binding with game logic. You **should always** unit test the game's logic, either isolating them from their dependencies or replacing them with test doubles. However, when the game logic is dependent on UnityEngine API features, this ends up becoming unachievable, as UnityEngine API features, for the most part, cannot be replaced by test doubles due to lack of inheritance.

For this reason, it is good practice to isolate game logic from UnityEngine API features as much as possible in order to facilitate testing. Thus, the game logic can be easily tested and its counterpart – usually a MonoBehaviour – will be dependent on the game logic (as well as the UnityEngine API features), not the other way around. By doing so, the game logic can be included in the unit tests and its counterpart is mostly left for integration tests.

>See more about isolating game logic from Unity-bound behaviour [here](./design-patterns-and-testing.md#humble-object-pattern).