# Code Coverage

Code Coverage is a measure of how much of your code has been executed. It is normally associated with automated tests, but you can gather coverage data in Unity at any time when the Editor is running. Using the Code Coverage package with the Test Runner, we can gather and present test coverage information as a report that shows the percentage of the code that has been executed.

For more information about Code Coverage package, click [here](https://docs.unity3d.com/Packages/com.unity.testtools.codecoverage@1.1/manual/index.html).

## Setting up Code Coverage

To install the Code Coverage, use the Unity Package Manager to find and install the package. To verify that Code Coverage has been installed correctly, open the Code Coverage window (go to **Window** > **Analysis** > **Code Coverage**). If you don't see the **Code Coverage** menu item, then Code Coverage did not install correctly.

To enable Code Coverage, open the Code Coverage window (go to **Window** > **Analysis** > **Code Coverage**) and select **Enable Code Coverage** – if not already selected – to be able to generate Coverage data and reports. Also, make sure **Generate HTML Report** and **Generate History** are checked.

>Enabling Code Coverage adds some overhead to the editor and can affect the performance.

Furthermore, you should **only** select the game assemblies as **Included Assemblies**, not the testing ones – as its not necessary to include in the report whether the test code is being reached. To exclude code from the Code Coverage, you can manually select the **Excluded Paths** or you can add the `[UnityEngine.TestTools.ExcludeFromCoverage]` attribute to an Assembly, Class, Constructor, Method or Struct script.

## Generating testing reports

To generate Code Coverage for tests – **after** running all tests –, open the Code Coverage window (go to **Window** > **Analysis** > **Code Coverage**) and click **Generate from Last**. Once ready, a file viewer window will open up containing the coverage report. 

### Summary view

In the `Report` folder, open `index.htm` in a web browser. This shows a summary of the coverage results from the tests containing the following sections:

The **Summary** section shows a brief summary of the coverage results including the number of assemblies, classes, files and lines that were processed. The most important value is the **Line Coverage** which shows the current coverage percentage of all coverable lines. Coverable lines only include the lines that can get executed and aren't marked to be excluded from coverage.

The **Coverage History** section will only appear if you enabled the **Generate History** option in the Code Coverage window. There you will see a graph showing the total percentage coverage for every test run for this project. Aim to keep this percentage as high as possible. If it is decreasing, consider writing more tests to improve your coverage.

The **Coverage** section shows a list of the assemblies that have been covered together with some stats showing how well covered they are. Select the **+** button next to the assembly name to see a list of the classes or structs within the assembly and their associated coverage data.

### Class/Struct View

To see more detailed information for a particular class, select its name in the **Summary** view **Coverage** section list. Then, you will see the following sections:

The **Summary** section, similar to the Summary section of the previous page, shows some brief statistics for the selected class. The most important value is the **Line Coverage** percentage. Select the **<** button in the top left hand corner to return to the previous screen.

The **Coverage History** section will only appear if you enabled the **Generate History** option in the Code Coverage window. It shows a graph of the coverage percentage of the class/struct over time. Try to keep this value as high as you can. Make sure that as you add new code the coverage percentage is maintained by adding more tests.

The **Metrics** section displays a list of the methods and properties of the class, along with each method's Cyclomatic Complexity, Crap Score and Sequence Coverage scores. Currently, the NPath Complexity and Branch Coverage aren't calculated, so will always appear as zero.

The **File(s)** section shows the C# source code for the selected class. Each line will be colored either green or red depending on whether the line was covered or not. The number in the left column indicates the number of times that the line was executed during the tests or the Coverage Recording session.