# NSubstitute

NSubstitute is a friendly substitute for . NET mocking libraries. It has a simple, succinct syntax to help developers write clearer tests. NSubstitute is designed for Arrange-Act-Assert (AAA) testing and with Test Driven Development (TDD) in mind.

## Adding NSubstitute to Unity Project

You can get NSubstitute as a NuGet package. To install a NuGet package on a Unity Project, you can use [NuGetForUnity](https://github.com/GlitchEnzo/NuGetForUnity), a NuGet client built from scratch to run inside the Unity Editor. NuGetForUnity provides a visual editor window to see available packages on the server, see installed packages, and see available package updates.

Once you installed NSubstitute package, you will need to go to the assembly definitions generated for the Unity Test Framework (both **PlayModeTesting.asmdef** and **EditModeTesting.asmdef**) and add the references to the obtained NSubstitute package .DLLs: **Castle.Core.dll**, **NSubstitute.dll** and **System.Threading.Tasks.Extensions.dll**.

## Creating Mocks and Stubs

Given the next scenario,

```c#
    //Incoming data dependency
    public interface IDatabase
    {
        string[] GetAllFriendsOfUser(string username);
    }

    //Outcoming data dependency
    public interface IServerMessager
    {
        bool SendMessage(Message message, params string[] username);
    }

    //Immutable Dependency
    public struct Message
    {
        public readonly string SenderUsername;
        public readonly string MessageText;

        public Message(string senderUsername, string messageText)
        {
            SenderUsername = senderUsername;
            MessageText = messageText;
        }
    }

    //System to be tested
    public class UserMessager
    {
        private string _username;
        private IDatabase _database;
        private IServerMessager _serverMessager;

        public UserMessager(string username, IDatabase database, IServerMessager serverMessager)
        {
            _username = username;
            _database = database;
            _serverMessager = serverMessager;
        }

        public void SendMessageToUser(string messageText, string recipientUsername)
        {
            Message message = new Message(_username, messageText);
            _serverMessager.SendMessage(message, recipientUsername);
        }

        public void SendMessageToAllFriends(string messageText)
        {
            string[] recipientUsernames = _database.GetAllFriendsOfUser(_username);
            Message message = new Message(_username, messageText);

            _serverMessager.SendMessage(message, recipientUsernames);
        }
    }
```

to test the behaviour of sending messages to a specif user, we need to isolate the *collaborators*. In this case, we can substitute them with test doubles:

```c#
    [Test]
    public void ShouldSendMessageToUser()
    {
        //Arrange
        IDatabase databaseDummy = Substitute.For<IDatabase>();
        databaseDummy.GetAllFriendsOfUser(Arg.Any<string>()).Returns((string[])null);

        IServerMessager serverMessagerSpy = Substitute.For<IServerMessager>();

        UserMessager userMessager = new UserMessager("Bob", databaseDummy, serverMessagerSpy);

        //Act
        userMessager.SendMessageToUser("Hello", "Jeff");

        //Assert
        serverMessagerSpy.Received().SendMessage(
            Arg.Is<Message>(message => message.SenderUsername == "Bob" && message.MessageText == "Hello"),
            Arg.Is<string>(username => username == "Jeff"));
    }
```

In the case above, the `IDatabase` test double is qualified as a **dummy**, since it is passed just to satisfy the SUT's constructor signature and doesn’t participate in producing the final outcome. On the other hand, the `IServerMessager` test double is qualified as a **spy**, since it is passed to emulate the dependency and to examine the **outcoming** interaction – asserting that the SUT actually called the ServerMessager to send a `"Hello"` message, sended by `"Bob"` to the user `"Jeff"` (expected behaviour).

However, to test behaviour of sending messages to all user friends, we need to actually interact with the `IDatabase` test double, like this:

```c#
    [Test]
    public void ShouldSendMessageToAllFriends()
    {
        //Arrange
        string[] friends = new string[] { "Jeff", "Johnny" };

        IDatabase databaseStub = Substitute.For<IDatabase>();
        databaseStub.GetAllFriendsOfUser("Bob").Returns(friends);

        IServerMessager serverMessagerSpy = Substitute.For<IServerMessager>();

        UserMessager userMessager = new UserMessager("Bob", databaseStub, serverMessagerSpy);

        //Act
        userMessager.SendMessageToAllFriends("Hello everyone");

        //Assert
        serverMessagerSpy.Received().SendMessage(
            Arg.Is<Message>(message => message.SenderUsername == "Bob" && message.MessageText == "Hello everyone"),
            Arg.Is<string[]>(usernames => usernames == friends));
    }
```

In the case above, the `IDatabase` test double is now qualified as a **stub**, since it is used to emulate a **incoming** interaction, used to return a "fake" list of friends. Once again, the `IServerMessager` test double work as a **spy**, asserting that the SUT actually called the ServerMessager to send a `"Hello everyone"` message, sended by `"Bob"` to all his friends: `"Jeff"` and `"Johnny"` (expected behaviour).

To avoid having hardcoded values in the test, you can use the `[TestCase]` to specify multiple test cases, using specific values passed as argument. On the test method, you can set the parameters with will be used later:

```c#
    [TestCase("Bob", "Hello everyone", new string[] { "Jeff", "Johnny" })]
    public void ShouldSendMessageToAllFriends(string senderUsername, string messageText, params string[] recipientUsernames)
    {
        //Arrange
        IDatabase databaseStub = Substitute.For<IDatabase>();
        databaseStub.GetAllFriendsOfUser(senderUsername).Returns(recipientUsernames);

        IServerMessager serverMessagerSpy = Substitute.For<IServerMessager>();

        UserMessager userMessager = new UserMessager(senderUsername, databaseStub, serverMessagerSpy);

        //Act
        userMessager.SendMessageToAllFriends(messageText);

        //Assert
        serverMessagerSpy.Received().SendMessage(
            Arg.Is<Message>(message => message.SenderUsername == senderUsername && message.MessageText == messageText),
            Arg.Is<string[]>(usernames => usernames == recipientUsernames));
    }
```

To learn more NSubstitute features, click [here](https://nsubstitute.github.io/).