# Unity Test Framework

The Unity Test Framework (UTF) its a package that provides a standard test framework that enables Unity users – users of Unity and developers at Unity – to test their code in both **Edit Mode** and **Play Mode**, and also on target platforms such as Standalone, Android, iOS, etc. UTF uses a Unity integration of NUnit library, which is an open-source unit testing library for .NET languages.

For more information about Unity Test Framework, click [here](https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/manual.html).

## Summary

1. [Edit Mode vs Play Mode tests](#edit-mode-vs-play-mode-tests)
2. [Setting up Unity Test Framework](#setting-up-unity-test-framework)
3. [Creating test scripts](#creating-test-scripts)
4. [Running a test](#running-a-test)
5. [Known limitations](#known-limitations)

## Edit Mode vs Play Mode tests

**Edit Mode** tests (also known as Editor tests) are only run in the Unity Editor and have access to the Editor code in addition to the runtime code. With Edit Mode tests it is possible to test any Editor extensions using the `UnityTest` attribute. For Edit Mode tests, the test code runs in the `EditorApplication.update` callback loop.

**Play Mode** tests can be run as a standalone in a Player or inside the Editor. Play Mode tests allow you to exercise your game code, as the tests run as coroutines if marked with the UnityTest attribute. 

## Setting up Unity Test Framework

Unity Test Framework can be installed through Unity Package Manager. To access the Unity Test Framework (UTF) in the Unity Editor, open the **Test Runner** window (**Window** > **General** > **Test Runner**). Through the Test Runner, you can set up `TestAssemblies` – assemblies that references NUnit, inside which Unity Test Framework will look for test scripts. **Play Mode** and **Edit Mode** tests need to be in separate assemblies.

>***Warning***: Before creating the testing assemblies, follow the instructions for organizing the project's directory structure [here](./project-setup-organization.md).

Once the assembly is created – and selected –, in the Inspector window, it should have references to **nunit.framework.dll**, **UnityEngine.TestRunner**, and **UnityEditor.TestRunner** assemblies – as well as **Editor** preselected as a target platform for the **Edit Mode** assembly. However, you will also need to add references to whatever dependencies (and tools) you will use in your tests – such as the **game code** assembly and the **mocking tool** assembly.

## Creating test scripts

>***Warning***: Before creating test scripts, follow the instructions for organizing the project's directory structure [here](./project-setup-organization.md#game-and-testing-assembly-definitions).

You can create a test script, based on Unity's default template, through the Test Runner – when for the first time – or by navigating to **Assets** > **Create** > **Testing** > **C# Test Script**. However, you can normally create a C# script inside the desired assembly to write a new test – once you used the `[Test]` or `[UnityTest]` attributes, Unity Test Framework will include those automatically.

## Running a test

You can run a test though the **Test Runner** window, either by double-clicking on the test or test fixture name, the top bar actions or using a context menu option **Run**. When doing so, all children of an item in the test tree will execute (if any). As a result, the test status icon will change and a counter in the top right corner will be updated. If a test fails, you can click on it and see, at the very bottom, the test log.

## Known limitations
Unity Test Framework version 1.0.18 includes the following known limitations:

- The `[UnityTest]` attribute does not support WebGL and WSA platforms.
- The `[UnityTest]` attribute does not support Parameterized tests (except for `ValueSource`).
- The `[UnityTest]` attribute does not support the NUnit Repeat attribute.
- Nested test fixture cannot run from the Editor UI.
- When using the NUnit `[Retry]` attribute in PlayMode tests, it throws `InvalidCastException`.
- Async tests are not supported in the current version of UTF.