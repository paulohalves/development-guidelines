# Design Patterns and Testing

There are some design patterns that make it easier, and sometimes capable, to write tests. In this session, we'll look at some design patterns and the advantages of each for writing tests.

## Summary
1. [SOLID pattern](#solid-pattern)
    1. [Single-responsibility principle](#single-responsibility-principle)
    2. [Open-closed principle](#open-closed-principle)
    3. [Liskov substitution principle](#liskov-substitution-principle)
    4. [Interface segregation principle](#interface-segregation-principle)
    5. [Dependency inversion principle](#dependency-inversion-principle)
2. [Dependency Injection Pattern](#dependency-injection-pattern)
3. [Humble Object Pattern](#humble-object-pattern)

## SOLID pattern

SOLID is a acronym for five design principles intended to make software designs more understandable, flexible, and maintainable. Each principle gives certain advantages to writing tests.

### Single-responsibility principle

The single-responsibility principle (SRP) is a computer-programming principle that states that every module, class or function in a computer program should have responsibility over a single part of that program's functionality – or behaviour –, and it should encapsulate that part.

When writing tests, whether unit or integration, this principle helps to identify – and isolate – the behaviors to be tested. That is, when we write tests for a class (system under test), we write separate tests for that class; when we write the test for a behavior, we only test functions (or methods) that are really relevant to that behavior; and so on.

### Open-closed principle

 The open-closed principle (OCP) states every software entity (classes, modules, functions, etc.) should be open for extension, but closed for modification, that is, such an entity can allow its behaviour to be extended without modifying its source code.

The idea here is that, in the need to expand the behavior, this principle allows you to extend the behavior without modifying it and also without compromising the source code that has already been implemented and tested. In this way, the written test can also be expanded and the test for the source code does not need to be modified.

### Liskov substitution principle

The Liskov substitution principle (LSP) states that objects of a superclass shall be replaceable with objects of its subclasses without breaking the system behaviour. That requires the objects of your subclasses to behave in the same way as the objects of your superclass, that is, an overridden method of a subclass needs to accept the same input parameter values as the method of the superclass, the return value of a method of the subclass needs to comply with the same rules as the return value of the method of the superclass, etc.

The idea here is similar to the previous principle, in the need to expand the behavior (i.e. creating subclasses), this principle allows to extend without compromising source code (i.e. superclasses) that has already been implemented and tested. In this way, the written test can also be expanded and the source code test does not need to be modified.

### Interface segregation principle

The interface segregation principle (ISP) states that no code should be forced to depend on methods it does not use. So, spliting interfaces that are very large into smaller and more specific ones helps clients to only have to know about the methods that are of interest to them.

Similarly to the single-responsibility principle, this principle helps to isolate the methods that are really relevant to that behavior. In addition, this principle helps with the use of test doubles, since, in this way, you only need to worry about substituting specific methods (and not all methods of a large interface).

### Dependency inversion principle

The dependency inversion principle states that high-level modules should not depend on low-level modules, both should depend on abstractions, and those abstractions should not depend on details, as details should depend on abstractions.

Similarly to both Open–closed and Liskov substitution principles, in the need to expand the behavior (i.e. creating/using new low-level modules), this principle allows to extend without compromising high-level modules that has already been implemented and tested. In this way, the written test can also be expanded and the high-level test does not need to be modified.

## Dependency Injection Pattern

Dependency Injection is a design pattern in which the creation and binding of dependencies are done outside of the dependent class, that is, the dependent class **should not** instantiate (or assing) its dependencies internally – and have to know how to configure them – thus causing coupling.

The idea here is that this pattern allows you to completely isolate the System Under Test from its dependencies – if the SUT receives its dependencies instead of constructing them, the test can *inject* fake dependencies (test doubles) to substitute them.

## Humble Object Pattern

When writing tests for games in Unity, we will encounter situations where some SUTs (system under test) will necessarily have collaborators (either mutable or shared dependencies), mainly those that use UnityEngine API services (such as `Collider`, `Rigidbody`, `CharacterController`, etc).

Situations like this make it very hard to write tests:
- It can be difficult to instantiate a MonoBehaviour for unit testing, as logic is located deep among Unity features.
- It can be difficult to isolate the behaviors to be tested, as MonoBehaviours tend to have mostly asynchronous functionality – thus making it difficult to use the Arrange, Act and Assert pattern.
- It can be difficult to replace dependencies (especially Unity features) with test doubles, as in cases like these, testing the behavior of "player must be able to walk" may not have a valid meaning when such dependencies are not are included in the tests. Besides, most Unity classes cannot be replaced by test doubles, due to lack of inheritance.
- Et cetera.

The Humble Object Pattern is a good solution because by using this pattern we extract the logic into a separate, easy-to-test component that is decoupled from its hard-to-test environment. This component consisting of methods (usually by implementing a service interface) that expose all the logic of the untestable component (preferably accessible via synchronous method calls).

> In a test-driven environment, behaviors based on synchronous methods are always preferable, as this makes both the system and the tests more understandable – in addition to making the tests more readable and faster.

As a result, the rest of the code with the hard-to-test functionality becomes the Humble Object: a very thin adapter layer that contains only code related to the UnityEngine API. Each time the Humble Object is called by the framework, it delegates to the testable component. Thus, we can include the easy-to-test component in the unit tests (now with the behaviors isolated from collaborators) and leave the Humble Object only for integration tests (along with collaborators and the logic component).