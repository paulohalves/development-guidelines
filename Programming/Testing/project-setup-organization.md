# Project Setup and Organization

To start testing the project's code, we need to install and setup all the mandatory tools: [Unity Test Framework](./unity-test-framework.md#unity-test-framework), [NSubstitute](./nsubstitute.md#nsubstitute) and [Code Coverage](./code-coverage-tool.md#code-coverage). Once all tools are installed, we need to setup some assembly definitions and organize the project's directory structure like this:

```
Assets/
{
    ...
    _Project/
    {
        ...
        Runtime/
        {
            ...
            Scripts/
            {
                ...
                Game.asmdef
            }
        }
        Testing/
        {
            ...
            Scripts/
            {
                ...
                EditMode/
                {
                    IntegrationTesting/
                    {
                        ...
                    }
                    UnitTesting/
                    {
                        ...
                    }
                    EditModeTesting.asmdef
                }
                PlayMode/
                {
                    IntegrationTesting/
                    {
                        ...
                    }
                    UnitTesting/
                    {
                        ...
                    }
                    PlayModeTesting.asmdef
                }
                Testing.asmdef
            }
        }
    }
}
```

Next, we will see explanations of why and how to achieve this organization.

## Game and Testing assembly definitions

In order for us to access the game's source code references/dependencies when writing the tests, we'll need to create an assembly definition for it. To do this, you need to right-click on `Assets/_Project/Runtime/Scripts`, go to **Create** and click on **Assembly Definition**.

>**Encapsulation**: When writing game scripts, you should encapsulate them into a proper namespace – preferably with the same name as the assembly definition, e.g. namespace `Game` for the `Game.asmdef` assembly.

After creating an assembly definition for game's source code, we need to create testing ones. First, we need to create an assembly definition for general testing code, by right-clicking on `Assets/_Project/Testing/Scripts`, going to **Create** and clicking on **Assembly Definition**. Then, name it **Testing.asmdef**. This assembly is important for isolating test code from runtime source code.

>**Encapsulation**: When writing general test scripts, you should encapsulate them into the namespace `{game namespace}.Tests`, e.g. `Game.Tests`.

Next, we need to create the assembly definitions for the Test Framework, as guided by [here](./unity-test-framework.md#setting-up-unity-test-framework). For **Edit Mode**, we need to create the assembly definition at path `Assets/_Project/Testing/Scripts/EditMode/` and name it **EditModeTesting.asmdef**. As for **Play Mode**, we need to create the assembly definition at path `Assets/_Project/Testing/Scripts/PlayMode/` and name it **PlayModeTesting.asmdef**.

After the assembly definitions for the Test Framework are created, we need to reference the **Testing.asmdef** and the game's source code assembly definition. Once that is done, it will look like this:

![Testing Assembly References](media/testing-assembly-references.png)

## Unit and Integration testing organization

Next, we need to organize our test scripts, separating them into unit testing and integration testing. To do this, all unit test scripts should be on the path `Assets/_Project/Testing/Scripts/{Mode}/UnitTesting/` and all integration test scripts should be on the path `Assets/_Project/Testing/Scripts/{Mode}/IntegrationTesting/`.

>**Encapsulation**: When writing test scripts, you should encapsulate them into proper namespaces: `{game namespace}.Tests.UnitTesting` for unit test scripts and `{game namespace}.Tests.IntegrationTesting` for integration test scripts.

This will be important for setting up all tests and isolating unit tests from integration tests – as most of the time they will run separately. Once we have our test scripts properly organized and encapsulated, **Test Runner** will look like this:

![Encapsulated Tests](media/encapsulate-tests.png)