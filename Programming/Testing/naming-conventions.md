# Naming Conventions and Pratices

Naming convention is a set of rules that help identify the **purpose** of classes, methods, and assets. Generally speaking, a naming convention:

- Helps to formalize and promote consistency within a team
- Improves clarity in case of ambiguity
- Avoid naming conflicts
- Allows you to transfer the project to another team with reduced costs
- Improves understanding for everyone

## Naming scripts and classes

When creating a new test, keep in mind that it will be used to test the behavior of a system. In object-oriented programming, systems will generally be represented by classes, e.g. `PlayerController`, `DatabaseManager`, `PlayerInventory`, etc.

In this case, your test script should have the same name as the system it is testing, plus "**.test**" added before the extension, that is, `{system}.test{extension}`.

The same rule applies for naming classes, however the class name will not contain `.test`, of course. Instead, the class must be **encapsulated** in the proper *namespace*: `{game namespace}.Tests.UnitTesting` for unit test scripts and `{game namespace}.Tests.IntegrationTesting` for integration test scripts.

For example, the unit test script for the `PlayerController.cs` will be named `PlayerController.test.cs`. Also, the class will be named and encapsulated like this:

```c#
    namespace Game.Tests.UnitTesting
    {
        public class PlayerController
        {
            ...
        }
    }
```

Furthermore, when writing general test scripts, such as utility scripts, you should name them normally – as you would with game utility scripts. However, besides being in the `Testing` assembly, they must be encapsulated by the proper namespace: `{game namespace}.Tests`. For example, the script called `TestingUtility.cs` should be written like this:

```c#
    namespace Game.Tests
    {
        public static class TestingUtility
        {
            ...
        }
    }
```

## Naming methods

Again, when writing a new test, remember that it will be used to test the **behaviours** of a system, not necessarily a **method**. So the test cases you will write are not necessarily reflect in the methods of the system under test – nor should they be, as this may indicate [coupling](./testing-patterns.md#never-couple-tests-to-the-implementation-logic).

In this case, when writing a new case (new test method), name it based on the expected behavior (which is being tested). Also, start the method with the term "**Should**" – or "**ShouldNot**" – thus indicating an expected state. Also use terms like "**When**", "**If**", to indicate a conditional behavior. By using these terms, we clearly express what behavior is expected, such as: "**should** do something", "**should** *be able to* do something", "**should** do something **when** condition satisfies", "**should not** *be able to* do something **if** condition satisfies", etc.

>There is no need to add the term "**BeAbleTo**" (`ShouldBeAbleTo`, `ShouldNotBeAbleTo`), as "**Should**" and "**ShouldNot**" are sufficient to express the intention.

For example:

```c#
    namespace Game.Tests.UnitTesting
    {
        public class PlayerController
        {
            [Test]
            public void ShouldJump()
            {
                ...
            }

            [Test]
            public void ShouldNotRunWhenCrouching()
            {
                ...
            }
        }
    }
```

Also, always suppress the system under test from the method name, as the SUT is already indicated in the class name. Given the next scenario,

```c#
    namespace Game.Tests.UnitTesting
    {
        public class PlayerController
        {
            //wrong away
            [Test]
            public void PlayerShouldJump()
            {
                ...
            }

            //right way
            [Test]
            public void ShouldNotRunIfCrouching()
            {
                ...
            }
        }
    }
```

the first case – the wrong way – will show on the TestRunner as "`PlayerController.PlayerShouldJump`", as the second case – the right way – will show as "`PlayerController.ShouldNotRunIfCrouching`".

## Writing assertions

When writing assertions, we should [always prioritize readability over brevity](./testing-patterns.md#always-prioritize-readability-over-brevity). To achieve this, we must **always** use the assertion utilities of the NUnit framework and in order to express it more clearly. The NUnit framework contains several helper classes with properties and methods that supply a number of constraints used in Asserts – such as `Is`, `Has`, `Does`, etc. Here are some examples:

```c#
    //always use
    Assert.That(player.Health, Is.EqualTo(0));
    //over
    Assert.AreEqual(player.Health, 0);
    ...

    //always use
    Assert.That(distance, Is.GreaterThanOrEqualTo(3));
    //over
    Assert.GreaterOrEqual(distance, 3);
    ...

    //always use
    Assert.That(items, Has.None.Null);
    //over
    foreach(var item in items)
    {
        Assert.NotNull(item);
    }
    ...

    //always use
	Assert.That(items, Does.Not.Contain(forbiddenItem));
    //over
    foreach(var item in items)
    {
        Assert.AreNotEqual(item, forbiddenItem);
    }
    ...
```

When in doubt about which form to use, follow this steps:
1. ***Think*** about the **behavior** being tested and its expected **outcome**, e.g. "the player **should** die when leaving the arena", "the inventory **should** delete items", "the player **should** move slowly when hurt", etc.
2. ***State*** that a certain observable element must present itself – or not – in an expected way, e.g. "the player's health **is** equal to zero", "the inventory **does not** contain this item", "the distance the player moved **is** less than when unharm", etc.
3. ***Write*** the assertion in a way that translates your previous statement. If this is not possible somehow, try to get as close to the statement as possible, e.g. "`Assert.That(playerHealth, Is.EqualTo(0))`", "`Assert.That(inventoryItems, Does.Not.Contain(deletedItem))`", "`Assert.That(distanceMovedHurt, Is.LessThan(distanceMovedUnharm))`", etc.
4. ***Read*** the assertion expression aloud. If the expression is confusing, it may be a sign that it is not as readable as it could be. In that case, you may need to ***rewrite*** your assertion. The idea is that anyone can read and understand the assertion, without worrying about how the test was written and, mainly, how the system was coded.

However, there is an **exception** to the rule when writing assertions for **test doubles**, using **NSubstitute**. Unfortunately, asserting expressions from a `Substitute` may not bring as much readability as the ideal. In this case, be patient and try to do your best.

## Naming humble objects

When using the [**Humble Object Pattern**](./design-patterns-and-testing.md#humble-object-pattern), you will need to separate your system into two components (two classes/scripts): the easy-to-test component – containing the system logic – and the hard-to-test component – containing code related to the UnityEngine API and other collaborators).

The most common naming convention for these components, adopted by the Unity community, is to name the easy-to-test component as `{system}Logic` – as it contains all the logic – and the hard-to-test component as `{system}Behaviour` – as it usually inherits from a MonoBehaviour. For example: applying the **Humble Object** pattern to a script called "`PlayerController.cs`" will separate it into "`PlayerControllerLogic.cs`" and "`PlayerControllerBehaviour.cs`".