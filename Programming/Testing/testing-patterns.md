# Testing Patterns 

## Unit vs Integration testing

You should **always** prioritize unit tests over integration tests, in the sense that unit tests are faster and isolated from their dependencies. However, that **does not** mean you don't need to do integration tests when unit tests already cover the code of certain systems.

Furthermore, there will be cases where some behaviors contain certain dependencies that are difficult to isolate/mock, or where the interaction between multiple systems is the behavior to test. In cases like this, integration tests are **mandatory**, as unit tests alone will not be enough.

In general, always try to **isolate** the system logic from `MonoBehaviours` (see more [here](./design-patterns-and-testing.md#humble-object-pattern)) and then write the **unit tests** for that logic component. Otherwise, leave the systems that inherit from `MonoBehaviour` (or that are too bound to Unity stuff) for the **integration tests**.

## Test-driven development (TDD)

**Test-driven development** is a software development process that relies on tests to drive the project development. The process consists of three stages, which you repeat for every test case:
1. Write a failing test to indicate which functionality needs to be added and how it should behave.
2. Write just enough code to make the test pass. At this stage, the code doesn't have to be elegant or clean.
3. Refactor the code. Under the protection of the passing test, you can safely clean up the code to make it more readable and maintainable.

Following this process gives us a lot of advantages, like the following:
- We have clarity on which behaviors each system will be responsible for – and which behaviors we will have to test.
- It helps not to couple tests to implementation logic – since, at this point, we are only concerned with the expected behaviors and their outcomes.
- Helps to isolate the logic of hard-to-test environments – since there are several ways to implement a behavior, we often have the habit of using several dependencies to facilitate the implementation (like using MonoBehaviours in everything). However, we must do so in order to isolate them, always.

>**Keep in mind**, following this process should be **preferable**, not necessary.

## Unit testing private methods

You **should not** worry about testing private methods, for the following reasons:

- Exposing private methods leads to coupling tests to implementation details ([overspecification](#never-couple-tests-to-the-implementation-logic)).
- Tests should only be concerned with the expected **observable behavior**, not how that behavior is implemented internally. Private methods **should not** be tested directly, but **indirectly**, as part of the overarching observable behavior.
- If testing private methods as part of the observable behavior (indirectly) doesn't provide enough **coverage**, one of the two: the uncovered code is not being used and it may be better to exclude this code **or** is an indication of a missing abstraction that must be extracted in a separate class.

## Setupping SUT and test fixtures

>The following rules applies for the system under test (SUT).

A **test fixture** is an object the test runs against. This object can be a  dependency, data in the database, a file on the hard disk, etc. Such an object needs to remain in a known, **fixed** state before each test run, so it produces the same result.

Test fixtures arrangements take up too much space. It makes sense to extract these arrangements into separate methods or classes that you then reuse between tests. You can do that by extracting the common initialization code into **private factory methods** – and also shorten the test code, but at the same time keep *readability* in the tests. Moreover, to avoid coupling tests to each other, you should make the private methods **generic** enough. That is, allow the tests to specify how they want the fixtures to be created. For example:

```c#
    private Inventory CreateInventoryWithItem(Item item, int count)
    {
        Inventory inventory = new Inventory();
        inventory.AddItem(item, count);
        return inventory;
    }
```

>Note that in this particular example, there’s no need to introduce factory methods, as the arrangement logic is quite simple. View it merely as a demonstration.

There are situations where we can also reuse test fixtures – not just the arrangement code. You can reuse a test fixture if:
- The test fixture is used by all or almost all tests.
- The test fixture is based on Unity stuff – such as a MonoBehaviour, which usually requires a long process of instantiating and configuring the GameObject with its necessary components.

You can do that like the following example:

```c#
    private Inventory _inventory;

    private Inventory CreateInventoryWithItem(Item item, int count)
    {
        if (_inventory == null)
        {
            _inventory = new Inventory();
        }
        
        //Always reset the fixture state!
        _inventory.ClearAll();

        _inventory.AddItem(item, count);
        return _inventory;
    }
```

***Beware***, in these cases you should always reset the test fixture to it's initial state, like the example above (preferable), of after each test by using the `[Teardown]` attribute, like the following:

```c#
    [Teardown]
    public void ResetFixtures() 
    {
        if (_inventory != null)
        {
            _inventory.ClearAll();
        }
        ...
    }
```

## Working with time

When testing games for Unity, we will have many asynchronous processes that will perform tasks over time – i.e. **Coroutines**. In this case, we'll see action sections with behaviors like "do something for x seconds" to then assert the outcome, e.g. "walk forward for x seconds", "do something and wait x seconds", etc.

However, we **should not** await tasks to complete when testing, because tests should always be as **quick as possible**. So try to avoid asynchronous logic – using `Update()` with `Time.deltaTime`, for example – and prefer functional logic instead. And, if you need to test an asynchronous behavior as such, try to wait as little time as possible – either by asserting a fraction of an outcome (updating the state for a few frames) or by manipulating the `Time.timescale`.

>Integration tests can take a little longer than unit tests, but do it wisely.

## Excluding unnecessary code from coverage

There are some exceptions to what you need to cover or not cover when testing. These are cases you don't have to worry about whether or not you're covering it in tests:

- Constant and read-only types: These types do not have their states changed during system operation, e.g. enums, structs used only to store data, scriptable objects used only to store data, etc.
- Methods that just capture user inputs: Unless you are using Unity's Input System package, there is no way to simulate user inputs and therefore no way to test them. In this case, it is better to abstract the input capture from the system - which is always a good practice.

To exclude code from Code Coverage, you can manually add the scripts in **Excluded Paths** in the **Code Coverage** window, or you can add the `[UnityEngine.TestTools.ExcludeFromCoverage]` attribute to class, struct or method.

## Overall guidelines (always, prefer, avoid and never)

### Always prioritize readability over brevity
Unit tests are most valuable when they are easy to read. One should understand the expected outcome of a unit of behavior by simply reading the test code. Thus, always use cases such as `Assert.That(player.Health, Is.EqualTo(0))` over `Assert.AreEqual(player.Health, 0)`. See more of writing assertions [here](./naming-conventions.md#writing-assertions).

### Always use AAA pattern
The `AAA pattern` advocates for splitting each test into three parts: arrange, act, and assert. This pattern provides a simple, uniform structure for all tests in the suite. This uniformity is one of the biggest advantages of this pattern: once you get used to it, you can easily read and understand any test. That, in turn, reduces maintenance costs for your entire test suite.
-	In the `arrange` section, you bring the system under test (SUT) and its dependencies to a desired state.
-	In the `act` section, you call methods on the SUT, pass the prepared dependencies, and capture the output value (if any).
-	In the `assert` section, you verify the outcome. The outcome may be represented by the return value, the final state of the SUT and its collaborators, or the methods the SUT called on those collaborators.

Example:
```c#
    [Test]
    public void ShouldSumTwoValues()
    {
        //Arrange
        int firstValue = 3;
        int secondValue = 5;
        Calculator calculator = new Calculator();

        //Act
        int result = calculator.Sum(firstValue, secondValue);

        //Assert
        Assert.That(result, Is.EqualTo(8));
    }
```

>**Given-When-Then pattern**<br>
This pattern is very similar to AAA, however its main difference is that its structure is more readable by non-programmers. Thus, Given-When-Then is best suited for tests that are shared with non-technical people. This pattern also advocates for breaking the test down into three parts:
>- Given – Corresponds to the arrange section
>- When – Corresponds to the act section
>- Then – Corresponds to the assert section

### Always keep coverage percentage high
When running **TestRunner** tests along with **Code Coverage**, make sure the **Line coverage** value on the report adhere to the following rules:

- **Good** metrics: above 80%. The code is well covered.
- **Regular** metrics: between 70% and 80%. The code is being covered enough, but it could be better.
- **Bad** metrics: below 70%. The code is not being covered enough.

### Always use the NUnit Test
Use the NUnit `[Test]` attribute instead of the `[UnityTest]` attribute, unless you need to yield special instructions, in Edit Mode, or if you need to skip a frame or wait for a certain amount of time in Play Mode.

### Prefer mocking interfaces over classes

For starters, **NSubstitute** can only work with *virtual* members of the class that are overridable (`public virtual`, `protected virtual`, etc) in the test assembly, so any non-virtual code in the class **will actually execute**. 

It also means features like `Received()`, `Returns()`, `When()..Do()`, etc, will **not** work with these non-overridable members. Further, it may not actually run an assertion and end up passing, making tests confusing and unreliable. These features will work correctly with virtual members of the class, but we have to be careful to **avoid** the non-virtual ones.

### Avoid testing more than one outcome
Tests that have multiple assertions and fail make it difficult to understand why the test is failing. And as you can expect, testing multiple outcomes in a single unit test usually have vague test names. Also, having multiple arrange, act, and assert sections means the test verifies multiple units of behavior. Such a test structure is no longer a unit test but rather is an integration test.

### Avoid hardcoded values on tests

To avoid having hardcoded values in the test, you can use the `[TestCase]` to specify multiple test cases, using specific values passed as argument. On the test method, you can set the parameters with will be used later. For example:

```c#
    //prefer this
    [TestCase("Bob", "Hello everyone", new string[] { "Jeff", "Johnny" })]
    [TestCase("Jack", "", new string[] { "Bob", "Jeff", "Johnny" })]
    public void ShouldSendMessageToAllFriends(string senderUsername, string messageText, params string[] recipientUsernames)
    {
        //Arrange
        IDatabase databaseStub = Substitute.For<IDatabase>();
        databaseStub.GetAllFriendsOfUser(senderUsername).Returns(recipientUsernames);

        IServerMessager serverMessagerSpy = Substitute.For<IServerMessager>();

        UserMessager userMessager = new UserMessager(senderUsername, databaseStub, serverMessagerSpy);

        //Act
        userMessager.SendMessageToAllFriends(messageText);

        //Assert
        serverMessagerSpy.Received().SendMessage(
            Arg.Is<Message>(message => message.SenderUsername == senderUsername && message.MessageText == messageText),
            Arg.Is<string[]>(usernames => usernames == recipientUsernames));
    }
    
    //over this
    [Test]
    public void ShouldSendMessageToAllFriends()
    {
        //Arrange
        string[] friends = new string[] { "Jeff", "Johnny" };

        IDatabase databaseStub = Substitute.For<IDatabase>();
        databaseStub.GetAllFriendsOfUser("Bob").Returns(friends);

        IServerMessager serverMessagerSpy = Substitute.For<IServerMessager>();

        UserMessager userMessager = new UserMessager("Bob", databaseStub, serverMessagerSpy);

        //Act
        userMessager.SendMessageToAllFriends("Hello everyone");

        //Assert
        serverMessagerSpy.Received().SendMessage(
            Arg.Is<Message>(message => message.SenderUsername == "Bob" && message.MessageText == "Hello everyone"),
            Arg.Is<string[]>(usernames => usernames == friends));
    }
```

### Never use if statements in tests
This is also an anti-pattern. A test — whether a unit test or an integration test — should be a simple sequence of steps with no branching. An if statement indicates that the test verifies too many things at once. Such a test, therefore, should be split into several tests. There are no benefits in branching within a test. if statements make the tests harder to read and understand.

### Never couple tests to the implementation logic
That is also called `overspecification`. Unit tests shouldn’t be concerned about the underlying logic, that is, tests should only be concerned with the expected behavior of a class or method, not how that behavior is implemented. If the test fails due to a change in implementation while the behavior and the application remains the same, it can be an indicator that you may need to re-examine your test.

### Never assert actual interactions with stubs
Since mocks help to emulate and examine **outcoming** interactions between the SUT and its dependencies, while stubs only help to emulate **incoming** interactions – not examine them –, asserting interactions with stubs is a anti-pattern that leads to fragile tests.

That is, its meaningful to check if the SUT made a call with specific parameters to a mock, because it corresponds to an actual outcome (as result of an expected behavior). However, its not meaningful to check if the SUT made a request to a stud, because its just a detail regarding how the SUT gathers data necessary to act.  It shouldn’t matter how the SUT generates the end result, as long as that result is correct (the SUT behaved as expected).

>The practice of verifying things that aren’t part of the end result is also `overspecification`. 

### Never imply implementation when writing tests

Also called `leaking domain knowledge`. You **should not** use nor duplicate the algorithm implementation from the production code. These tests are another example of coupling to implementation details. Such tests don't have a chance of differentiating legitimate failures from false positives. Instead of using/duplicating the algorithm, **hard-code** or **parameterize** its results into the test.

Example of hard-coding:
```c#
    //wrong way
    [Test]
    public void ShouldSumTwoValues()
    {
        //Arrange
        int firstValue = 3;
        int secondValue = 5;
        int expectedResult = firstValue + secondValue;
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.Sum(firstValue, secondValue);

        //Assert
        Assert.That(actualResult, Is.EqualTo(expectedResult));
    }

    //right way
    [Test]
    public void ShouldSumTwoValues(int firstValue, int secondValue, int expectedResult)
    {
        //Arrange
        int firstValue = 3;
        int secondValue = 5;
        int expectedResult = 8;
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.Sum(firstValue, secondValue);

        //Assert
        Assert.That(actualResult, Is.EqualTo(expectedResult));
    }
```

Example of parametizing:
```c#
    //wrong way
    [TestCase(3, 5)]
    public void ShouldSumTwoValues(int firstValue, int secondValue)
    {
        //Arrange
        int expectedResult = firstValue + secondValue;
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.Sum(firstValue, secondValue);

        //Assert
        Assert.That(actualResult, Is.EqualTo(expectedResult));
    }

    //right way
    [TestCase(3, 5, 8)]
    public void ShouldSumTwoValues(int firstValue, int secondValue, int expectedResult)
    {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.Sum(firstValue, secondValue);

        //Assert
        Assert.That(actualResult, Is.EqualTo(expectedResult));
    }
```

### Never add production code only for testing purposes

Also called `code pollution`. The problem with code pollution is that it mixes up test and production code and thereby increases the maintenance costs of the latter. To avoid this anti-pattern, keep the test code out of the production code base. For example, **never** do the following:

```c#
    public class MySystem
    {
        public bool IsTesting;

        public void DoBehaviour()
        {
            if (IsTesting)
            {
                //Do something
            }
            ...
        }
    }

```