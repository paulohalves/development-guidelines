# Unit testing

Unit testing is a software development process in which the smallest testable parts of an application, called units, are individually and independently tested, asserting proper operation.

## Summary
1. [Main concept](#main-concept)
2. [Classical vs London schools](#classical-vs-london-schools)
3. [Code Coverage](#code-coverage)
4. [Test doubles](#test-doubles)

## Main concept

There are three main points that define a unit test:
- A unit test verifies a single unit of behavior (generally a small piece of code)
- Does it quickly
- And does it in an isolated manner

However, there are nuances in defining a unit test that, the differences in interpreting them, have led to two distinct views on how to approach unit testing: the `Classical` school and the `London` school.

## Classical vs London schools

The main difference between the two schools are the definition of "isolation". For the `London` school, it is defined as "isolating the system under test from its `collaborators` ([see below](#dependencies))". It means if a class has a dependency on another class, or several classes, you need to replace all such dependencies with `test doubles`. This way, you can focus on the class under test exclusively by separating its behavior from any external influence.

Another benefit of this approach is allowing to introduce a project-wide guideline of testing only one class at a time, which establishes a simple structure in the whole unit test suite. By doing this, it becomes clear how to cover the codebase with tests.

For `Classical` approach, it’s not the code that needs to be tested in an isolated manner. Instead, unit tests themselves should be run in isolation from each other, that is, do not share any data or state (`shared dependencies`). That way, you can run the tests in parallel, sequentially, and in any order, whatever fits you best, and they still won’t affect each other’s outcome. Typical examples of such a shared information are out-of-process dependencies — the database, the file system, and so on.

Another reason for substituting shared dependencies is to increase the test execution speed and reachability. Shared dependencies almost always reside outside the execution process, while private dependencies usually don’t cross that boundary. Because of that, calls to shared dependencies (database,file system, external APIs, etc) take more time than calls to private dependencies, if reachable at all (online API).

<a id="dependencies"></a>
| Dependency Type | Description |
| - | - |
| Mutable Dependency | A dependency that change its data over time (*entities*). |
| Immutable Dependency | A dependency that has read-only data (*value* or *value object*). |
| Shared dependency | A dependecy that shares data between tests (static mutable field, public fields, database, file system, etc.). |
| Private Dependency | A dependency that do not share data between tests. | 
| Collaborators | Is either an mutable or shared dependency. |
| | |

> **Entities vs Value Objects** <br>
`Value objects` should be immutable in a sense that if we need to change such an object, we construct a new instance based on the existing object rather than changing it. On the contrary, `entities` are almost always mutable.

## Code Coverage

A *coverage metric* shows how much source code a test suite executes, from none to 100%. There are different types of coverage metrics, and they’re often used to assess the quality of a test suite. The first and most-used coverage metric is `code coverage`, also known as *test coverage*. This metric shows the ratio of the number of code lines executed by at least one test and the total number of lines in the production code base.

Another coverage metric is called `branch coverage`. Branch coverage provides more precise results than code coverage because it helps cope with code coverage’s shortcomings. Instead of using the raw number of code lines, this metric focuses on control structures, such as if and switch statements. It shows how many of such control structures are traversed by at least one test in the suite.

The common belief is that the higher the coverage number, the better. However, it does not effectively measure the quality of a test suite. Coverage metrics are a good negative indicator but a bad positive one. That is, if a metric shows that there’s too little coverage in your code base — say, only 10% — that’s a good indication that you are not testing enough. But the reverse isn’t true: even 100% coverage isn’t a guarantee that you have a good-quality test suite.

> ***Coverage metrics DO NOT guarantee that the test verifies all the possible outcomes*** <br>
For the code paths to be actually tested and not just exercised, unit tests must have appropriate assertions and relevant units of behavior under test.

## Test doubles

A test double is an overarching term that describes all kinds of non-production-ready, fake dependencies in tests. The major use of test doubles is to facilitate testing; they are passed to the system under test instead of real dependencies, which could be hard to set up or maintain. In general agreement, there are 5 types of test doubles:

| <div style="width:100px"> Test double type </div> | Description |
| - | - |
| Dummy | Dummy objects are immutable dependecies, hardcode values, used just to satisfy the SUT's method signature and doesn’t participate in producing the final outcome, i.e. parameters in a constructor which have *no effect* on your test. |
| Stub | Stubs emulate **incoming** interactions, used to return different values for different scenarios, i.e. calls the SUT makes to its dependencies to get input data. |
| Spy | Spies emulate and examine **outcoming** interactions, i.e. calls the SUT makes to its dependencies to change their state. |
| Mock | Mocks serve the same role as spies. The distinction is that spies are written manually, whereas mocks are created with the help of a *mocking framework*. Also, assertions are made inside mocks, whereas spies are usually not. |
| Fake | Fakes are objects that have working implementations, but not same as production one – usually they take some shortcut and have simplified version of production code, i.e. fake implementation of a database, except using in-memory collection. |
| | |

For easy understanding, they can all be grouped together into just two types: `mocks` (mocks and spies) and `stubs` (fakes, stubs and dummies).