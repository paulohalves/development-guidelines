# Development Guidelines

General development guidelines for working with Unity projects. This project contains guidelines for team development using the Unity engine. Throughout the project you will find folders that classify the content in order to facilitate the location of the desired one.

In addition, we will use keywords like `Always`, `Prefer`, `Avoid` and `Never`, which you should interpret as the following:

- **Always**: Mandatory rule to follow, that is, the practice in question must be followed at all times.
- **Prefer**: Considered a good practice.
- **Avoid**: Considered a bad practice.
- **Never**: Mandatory rule to follow, that is, the practice in question must be avoided at all costs.

## Summary

- [Programming Guidelines](./Programming/index.md)